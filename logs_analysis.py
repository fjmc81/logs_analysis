#!/usr/bin/env python3
import pep8
import psycopg2

#ALTER USER user_name WITH PASSWORD 'new_password';
# To custom the connection to db
hostname = '127.0.0.1'
username = 'vagrant'
mypasswd = '1234'
database = 'news'

db = psycopg2.connect(dbname=database, user=username, password =mypasswd, host=hostname)
conn = None

# query statements
articles = "SELECT articles.title, count(*) AS views FROM articles INNER JOIN log ON log.path LIKE concat('%', articles.slug, '%') WHERE log.status LIKE '%200%' GROUP BY articles.title, log.path ORDER BY views DESC LIMIT 3;"
authors = "SELECT authors.name, count(*) AS views FROM articles INNER JOIN authors ON articles.author = authors.id inner JOIN log ON log.path LIKE concat('%', articles.slug, '%') WHERE log.status LIKE '%200%' GROUP BY authors.name ORDER BY views DESC;"
errors = "SELECT day, perc FROM (SELECT day, round((sum(requests)/(SELECT count(*) FROM log WHERE substring(cASt(log.time AS text), 0, 11) = day) * 100), 2) AS perc FROM (SELECT substring(cASt(log.time AS text), 0, 11) AS day, count(*) AS requests FROM log WHERE status LIKE '%404%' GROUP BY day)AS log_percentage GROUP BY day ORDER BY perc desc) AS final_query WHERE perc >= 1;"

def most_popular_searches(query):

	try:
		conn = db
		cursor = conn.cursor()
		
		# execute the query statement
		query = cursor.execute(query)

		# get articles data
		statement_results = cursor.fetchall()
		
		for statement_result in statement_results:
			print ("\t", str(statement_result[0]),"\t - ", str(statement_result[1]), "views")
		
		return (statement_results)

	except (Exception, psycopg2.DatabaseError) as error:
		return(error)

def percent_errors_found(query):

	try:
		conn = db
		cursor = conn.cursor()
		query = cursor.execute(query)
		statement_results = cursor.fetchall()		
		for statement_result in statement_results:
			print ("\t", str(statement_result[0]),"\t - ", str(statement_result[1]), "%")
		return (statement_results)

	except (Exception, psycopg2.DatabaseError) as error:
		return(error)

if __name__ == '__main__':
	print("What are the most popular three articles of all time?")
	most_popular_searches(articles)
	print("Who are the most popular article authors of all time?")
	most_popular_searches(authors)
	print("On which days did more than 1% of requests lead to errors?")
	percent_errors_found(errors)
	if conn is not None:
			conn.close()
	

			