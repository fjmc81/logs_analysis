# Logs Analysis Project

This is still a draft version, so it is not a finish project yet.

## Getting Started

In this release, 

### Prerequisites

* **Environment** - *to run the .py file* - Postgres running under VM with Vagrant and python >3.5 installed
* **Txt editor** - ** - to see the txt file with the output example

### Installing

A step by step series of examples that tell you have to get a development env running

* Open a Terminal/Console/Prompt
* Type 'cd /go/to/folder/vagrant/.py file'
* Type 'python3 logs_analysis.py' to print out the result

Note: to setup a custom database password allocated in the VM created with Vangrant, please, follow these steps:

* psql -d news (to connect to db)
* \password (it will ask you for a new password and again for your confirmation)

## Built With

* [Vagrant](https://www.vagrantup.com/) - A tool for building and managing virtual machine environments in a single workflow.
* [Anaconda3](https://www.virtualbox.org/wiki/Downloads) - App needed by Vagrant to make run the VM
* Python 3 installed within VM created with Vagrant tool

## Versioning

v1.00 - Realease ready to be review by mentorship

## Authors

* **Francisco Javier Moreno Carracedo** - *Initial work* - [francisco-carracedo](https://francisco-carracedo.com)

## License

This project has been developer only with the purpose of serve as practise of study under Udacity 'full-stack-web-developer' nanodegree program.
